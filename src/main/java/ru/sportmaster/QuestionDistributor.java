package ru.sportmaster;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Scanner;

public class QuestionDistributor {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner questionScanner = new Scanner(new File("questions.txt"));
        ArrayList<String> questions = new ArrayList<>();
        while (questionScanner.hasNextLine()) {
            questions.add(questionScanner.nextLine());
        }
        questionScanner.close();

        Scanner nameScanner = new Scanner(new File("names.txt"));
        ArrayList<String> names = new ArrayList<>();
        while (nameScanner.hasNextLine()) {
            names.add(nameScanner.nextLine());
        }
        nameScanner.close();

        Collections.shuffle(questions);
        Collections.shuffle(names);

        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        String fileName = formatter.format(new Date()) + ".txt";
        PrintWriter writer = new PrintWriter(fileName);

        int size = Math.min(questions.size(), names.size());
        for (int i = 0; i < size; i++) {
            writer.println(names.get(i) + ": " + questions.get(i));
        }
        writer.close();
    }
}