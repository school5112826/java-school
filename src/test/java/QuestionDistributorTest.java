import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Scanner;
import org.junit.Test;
import ru.sportmaster.QuestionDistributor;

public class QuestionDistributorTest {

        @Test
        public void testFileExists() {
            // Check that file exists after running the utility
            try {
                QuestionDistributor.main(null);
                SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
                String fileName = formatter.format(new Date()) + ".txt";
                File file = new File(fileName);
                assertTrue(file.exists());
                file.delete();
            } catch (FileNotFoundException e) {
                fail("File not found exception: " + e.getMessage());
            }
        }

        @Test
        public void testFileNameFormat() {
            // Check that file name format is "dd.MM.yyyy.txt"
            try {
                QuestionDistributor.main(null);
                SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
                String dateString = formatter.format(new Date());
                String fileName = dateString + ".txt";
                File file = new File(fileName);
                assertTrue(file.exists());
                file.delete();
            } catch (FileNotFoundException e) {
                fail("File not found exception: " + e.getMessage());
            }
        }

        @Test
        public void testUniquePairs() {
            // Check that pairs are unique
            try {
                QuestionDistributor.main(null);
                SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
                String fileName = formatter.format(new Date()) + ".txt";
                File file = new File(fileName);
                Scanner scanner = new Scanner(file);
                ArrayList<String> pairs = new ArrayList<>();
                while (scanner.hasNextLine()) {
                    pairs.add(scanner.nextLine());
                }
                scanner.close();
                HashSet<String> uniquePairs = new HashSet<>(pairs);
                assertEquals(pairs.size(), uniquePairs.size());
                file.delete();
            } catch (FileNotFoundException e) {
                fail("File not found exception: " + e.getMessage());
            }
        }

        @Test
        public void testSameQuestionCount() {
            // Check that all questions are distributed
            try {
                QuestionDistributor.main(null);
                Scanner questionScanner = new Scanner(new File("questions.txt"));
                int questionCount = 0;
                while (questionScanner.hasNextLine()) {
                    questionScanner.nextLine();
                    questionCount++;
                }
                questionScanner.close();

                SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
                String fileName = formatter.format(new Date()) + ".txt";
                File file = new File(fileName);
                Scanner scanner = new Scanner(file);
                int pairCount = 0;
                while (scanner.hasNextLine()) {
                    scanner.nextLine();
                    pairCount++;
                }
                scanner.close();
                assertEquals(questionCount, pairCount);
                file.delete();
            } catch (FileNotFoundException e) {
                fail("File not found exception: " + e.getMessage());
            }
        }

    }
